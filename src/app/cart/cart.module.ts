import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';



import { SharedModule } from '../shared/shared.module';
import { CommonModule } from '@angular/common';
import { CartComponent } from './components/cart/cart.component';
import { CartRoutingModule } from './cart-rounting.module';



 

@NgModule({

declarations:[
    CartComponent

],
imports: [
    CommonModule,
    CartRoutingModule,
    FormsModule,
    SharedModule
  ]

})
export class CartModule {
 



}