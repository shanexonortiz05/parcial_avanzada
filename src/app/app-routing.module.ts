import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';


import { LayoutComponent } from './layout/layout.component';
import { AdminGuard } from './admin.guard';
import { OrderModule } from './order/order.module';



const routes: Routes = [
  {
    path: '',
    component: LayoutComponent,
    
    children: [
      {
        path:'',
        redirectTo: '/home',
        pathMatch: 'full',
      },

      {
        path: 'home', 
       loadChildren: () => import('./home/home.module').then(m => m.HomeModule)
      },
      {
        path: 'products',
        loadChildren: () => import('./product/product.module').then(m => m.ProductsModule)


      },

      {
        path: 'cart',
        loadChildren: () => import('./cart/cart.module').then(m => m.CartModule)


      },
     
      {
        path: 'contact',
        
      loadChildren: () => import('./contact/contact.module').then(m => m.ContactModule)
        
      },

      {
        path: 'order',
       // canActivate: [AdminGuard],
      loadChildren: () => import('./order/order.module').then(m => m.OrderModule)
        
      },

       {
    path: 'demo',
   
    loadChildren: () => import('./demo/demo.module').then(m => m.DemoModule)
  }
    ]
  },
  
  { 
    path: 'admin',
    canActivate: [AdminGuard],
    loadChildren: () => import('./admin/admin.module').then(m => m.AdminModule)
  },
  {
    path: 'auth',
    loadChildren: () => import('./auth/auth.module').then(m => m.AuthModule)
  },
  { 
    path: '**',
    loadChildren: () => import('./error/error.module').then(m => m.ErrorModule)
  }
  

];   

@NgModule({
  imports: [RouterModule.forRoot(routes,{
    preloadingStrategy: PreloadAllModules
  } )],
  exports: [RouterModule]
})
export class AppRoutingModule { }
