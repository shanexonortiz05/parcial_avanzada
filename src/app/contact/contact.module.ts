import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';


import { SharedModule } from '../shared/shared.module';
import { CommonModule } from '@angular/common';

import { ContactComponent } from './components/contact/contact.component';
import { ContactRoutingModule } from './contact-rounting.module';



 

@NgModule({

declarations:[
    ContactComponent

],
imports: [
    CommonModule,
    ContactRoutingModule,
    FormsModule,
    SharedModule
  ]

})
export class ContactModule {
 



}