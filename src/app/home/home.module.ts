import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BannerComponent } from './componentes/banner/banner.component';
import { HomeComponent } from './componentes/home/home.component';
import { HomeRoutingModule } from './home-rounting.module';
import { SharedModule } from '../shared/shared.module';
import { CommonModule } from '@angular/common';



 

@NgModule({

declarations:[
    BannerComponent,
    HomeComponent,

],
imports: [
    CommonModule,
    HomeRoutingModule,
    FormsModule,
    SharedModule
  ]

})
export class HomeModule {
 



}