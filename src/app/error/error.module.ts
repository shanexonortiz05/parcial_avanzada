import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';



import { SharedModule } from '../shared/shared.module';
import { CommonModule } from '@angular/common';
import { ErrorComponent } from './components/error/error.component';
import { ErroroutingModule } from './error-rounting.module';



 

@NgModule({

declarations:[
    ErrorComponent
],
imports: [
    CommonModule,
    ErroroutingModule,
    FormsModule,
    SharedModule
  ]

})
export class ErrorModule {
 



}