import { Component, OnInit } from '@angular/core';
import { CartService } from '../../../core/services/cart/cart.service';
import { Observable } from 'rxjs';
import { Product } from 'src/app/product.model';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.scss']
})
export class OrderComponent implements OnInit {

  products$ : Observable<Product[]>;
  displayedColumns = ['Imagen','Producto', 'Precio'];
  constructor(private cartService: CartService) { 

   this.products$ = this.cartService.cart$;

  }

  ngOnInit(): void {
  }

  addProduct(product: Product) {
    this.cartService.addCart(product)
  }
  // deleteProduct(id: string) {
  //   this.cartService.deleteProduct(id)
  // }


}
