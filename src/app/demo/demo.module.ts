import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';
import { FormsModule } from '@angular/forms';
import { DemoComponent } from './components/demo/demo.component';
import { DemoRoutingModule } from './demo-routing.module';
import { BrowserModule } from '@angular/platform-browser';



@NgModule({
  declarations: [DemoComponent],
  imports: [
    
    CommonModule,
    SharedModule,
    FormsModule,
    DemoRoutingModule
  ]
})
export class DemoModule { }
