import { Component, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'app-demo',
  templateUrl: './demo.component.html',
  styleUrls: ['./demo.component.scss']
})
export class DemoComponent implements OnInit {
 
  htmlVariable: string = "<b>Some html.</b>";
  title = 'platzi';
  power = 10;
  array = ['🍎', '🍏', '🍇', '🍌', '🍑'];
  cost = '200.13';
  constructor() { }

  ngOnInit(): void {
  }
  @ViewChild('input') private input;

  addText(){
    this.input.nativeElement.focus();
    let startPos = this.input.nativeElement.selectionStart;
    let value = this.input.nativeElement.value;
    this.input.nativeElement.value= 
    value.substring(0, startPos) +' <input> ' + value.substring(startPos, value.length)
  }
 
  reset(){
      this.input.nativeElement.value='';
  }

  addItem(){

    this.array.push('🍎🍌');
    console.log(this.array);
    
  }

  deleteItem(index: number){

      this.array.splice(index, 1);

  }

}
