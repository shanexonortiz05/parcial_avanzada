import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ProductsService } from '../../../core/services/products/products.service';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { MyValidators } from 'src/app/utils/validator';
import { Product } from '../../../product.model';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-product-edit',
  templateUrl: './product-edit.component.html',
  styleUrls: ['./product-edit.component.scss']
})
export class ProductEditComponent implements OnInit {

  form: FormGroup;
  id: string;
  constructor(private formBuilder: FormBuilder,
     private productService: ProductsService,
    private router: Router,
    private activeRoute:ActivatedRoute) { 
    this.buildForm();
  }

  ngOnInit() {

    this.activeRoute.params.subscribe((params: Params) => {

      this.id = params.id;
      this.productService.getProduct(this.id).subscribe(data=>{
        
        this.form.patchValue(data);

      })

    })

  }

  saveProduct(event: Event){
    event.preventDefault();
    
    
    if(this.form.valid){

      const product = this.form.value;
      this.productService.editProduct(this.id, product).subscribe(data=>{

        console.log(data);
        this.router.navigate(['/admin/products']);
        Swal.fire({
          position: 'top-end',
          icon: 'info',
          title: 'El producto a sido editado!',
          showConfirmButton: false,
          timer: 1500
        })
    
      }) 
    }
    console.log(this.form.value);
    

  }

  private buildForm(){

    this.form = this.formBuilder.group({

      id: ['',[Validators.required]],
      title:['',[Validators.required]],
      price: ['',[Validators.required, MyValidators.isPriceValid]],
      image:[''],
      description:['',[Validators.required]],

    })

  }

  get priceField(){

    return this.form.get('price');

  }
}
