import { Component, OnInit, ElementRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ProductsService } from '../../../core/services/products/products.service';
import { ActivatedRoute, Router } from '@angular/router';
import { MyValidators } from 'src/app/utils/validator';
import Swal from 'sweetalert2';
import { AngularFireStorage } from '@angular/fire/storage';
import 'firebase/storage';
import { finalize } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { url } from 'inspector';

@Component({
  selector: 'app-form-product',
  templateUrl: './form-product.component.html',
  styleUrls: ['./form-product.component.scss']
})
export class FormProductComponent implements OnInit {


    form: FormGroup;
    image$: Observable<any>;

  constructor(private formBuilder: FormBuilder,
     private productService: ProductsService,
    private router: Router,
    private route:ActivatedRoute,
    private storage: AngularFireStorage ) { 
    this.buildForm();
  }

  ngOnInit(): void {
  }

  saveProduct(event: Event){

 event.preventDefault();
    if(this.form.valid){

      const product = this.form.value;
      this.productService.createProduct(product).subscribe(data=>{
        Swal.fire({
          position: 'top-right',
          icon: 'success',
          title: 'El producto a sido creado',
          showConfirmButton: false,
          timer: 1500
        })
        console.log(data);
        this.router.navigate(['/admin/products']);
      }) 
    }
    console.log(this.form.value);
}

  uploadFile(event){

    const file = event.target.files[0];
    const name = 'image.png';
    const fileRef = this.storage.ref(name);
    const task = this.storage.upload(name, file);


    task.snapshotChanges().pipe(

      finalize(() => {
        this.image$ = fileRef.getDownloadURL();
        this.image$.subscribe(url => {
          this.form.get('image').setValue(url);

        })
      })
    ).subscribe();
    


  }

  private buildForm(){

    this.form = this.formBuilder.group({

      id: ['',[Validators.required]],
      title:['',[Validators.required]],
      price: ['',[Validators.required, MyValidators.isPriceValid]],
      image:[''],
      description:['',[Validators.required]],

    })

  }

  get priceField(){

    return this.form.get('price');

  }
}
