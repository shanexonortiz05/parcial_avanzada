import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2';
import { ProductsService } from '../../../core/services/products/products.service';

@Component({
  selector: 'app-products-list',
  templateUrl: './products-list.component.html',
  styleUrls: ['./products-list.component.scss']
})
export class ProductsListComponent implements OnInit {

  constructor(private productsService : ProductsService) { }
 products = [];
 displayedColumns: string[] = ['Id','Titulo','Precio','Acciones']
  ngOnInit(): void {
    this.getProducts();
  }


  getProducts(){

this.productsService.getAllProducts().subscribe(data => {

this.products = data;

})

  }
  deleteProducts(id: string){
    
Swal.fire({
  title: 'Esta Seguro que desea eliminar el producto?',
 // text: "No puede revertir !",
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Si, eliminar!'
}).then((result) => {
  if (result.isConfirmed) {
    this.productsService.deleteProduct(id).subscribe(data =>{
      this.getProducts();
     
console.log(data);
 })
    Swal.fire(
      'Eliminado!',
      'El producto a sido eliminado.',
      'success'
    )
  }
})

  }

}
