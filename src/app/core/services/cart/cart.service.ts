import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Product } from 'src/app/product.model';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CartService {

 private  products: Product[] = [];
private cart = new BehaviorSubject<Product[]>([]);

cart$ = this.cart.asObservable();

  constructor(private http : HttpClient) { }

addCart(product: Product){

  this.products = [...this.products, product];
  this.cart.next(this.products);
}



}
