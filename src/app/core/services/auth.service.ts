import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private angularFireAuth: AngularFireAuth) { }


  createUser(email: string, pass: string){

  return  this.angularFireAuth.createUserWithEmailAndPassword(email,pass);
  }

  login(email: string, pass: string){
    return this.angularFireAuth.signInWithEmailAndPassword(email, pass);
  }

  logout(){
    return this.angularFireAuth.signOut();
  }


  hasUser(){

  return  this.angularFireAuth.authState;
    
  }
}
