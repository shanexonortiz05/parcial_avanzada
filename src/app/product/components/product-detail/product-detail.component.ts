import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { ProductsService } from 'src/app/core/services/products/products.service';
import { Product } from 'src/app/product.model';




@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.scss']
})
export class ProductDetailComponent implements OnInit {


  product: Product;

  constructor(private route: ActivatedRoute, private productService: ProductsService) { }

  ngOnInit() {



    this.route.params.subscribe((params: Params) => {
      const id = params.id;
      //this.product =  this.productService.getProduct(id);
      this.getProductById(id);
      console.log(id);
    });

  }


  getProductById(id: string) {

    this.productService.getProduct(id).subscribe(data => {

      this.product = data;

    })

  }

  create() {
    const newProduct: Product = {
      id: '222',
      title: 'producto nuevo',
      image: 'assets\images\banner-1.jpg',
      price: 300,
      description: 'lalal'

    }

    this.productService.createProduct(newProduct).subscribe(data => {
    })

  }

  update() {

    const EditProduct: Partial<Product> = {

      price: 4000,
      description: 'edicion'

    }

    this.productService.editProduct('2', EditProduct).subscribe(data => {

      console.log(data);


    })

  }

  delete() {

    this.productService.deleteProduct('19').subscribe(data => {

      console.log(data);


    })

  }



}
