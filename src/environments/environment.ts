// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  url_api: 'https://platzi-store.herokuapp.com',
  firebase : {
    apiKey: "AIzaSyA99bgkVWVI1d2IoDzF3ZY2f8KOGxg14W4",
    authDomain: "platzi-store-21892.firebaseapp.com",
    projectId: "platzi-store-21892",
    storageBucket: "platzi-store-21892.appspot.com",
    messagingSenderId: "539275810054",
    appId: "1:539275810054:web:40f524ba9926dec705ba87"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
